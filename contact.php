<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, shrink-to-fit=no"
    />
    <meta name="description" content="" />
    <meta
      name="author"
      content="Mark Otto, Jacob Thornton, and Bootstrap contributors"
    />
    <meta name="generator" content="Jekyll v4.1.1" />
    <title>Advanced Platform Solutions</title>

    <link
      rel="canonical"
      href="https://getbootstrap.com/docs/4.5/examples/jumbotron/"
    />

    <link
      href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;600;700&display=swap"
      rel="stylesheet"
    />

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet" />

    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet" />

    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
    />

    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet" />

    <script
      src="https://kit.fontawesome.com/7e408bb136.js"
      crossorigin="anonymous"
    ></script>

    <link rel="icon" href="images/favicon.svg" />

    <script src="https://www.google.com/recaptcha/api.js" async defer></script>

    <!-- added -->
    <link rel="stylesheet" href="css/intlTelInput.css">
    <title>DWM - Chicago</title>
    <style>
        .g-recaptcha{
            display: inline-block;
        }
        .wrapper-recaptcha{
            text-align: center;
            margin-top:27px;
        }
    </style>

  </head>

  <body>
    <nav class="navbar navbar-expand-lg fixed-top navbar-light bg-light">
      <div class="container">
        <a class="navbar-brand" href="index.php"
          ><img src="images/logo-nav-aps.svg"
        /></a>
        <button
          class="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarsExample07"
          aria-controls="navbarsExample07"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarsExample07">
          <ul class="navbar-nav ml-auto purple">
            <li class="nav-item">
              <a class="nav-link" href="index.php">Home</a>
            </li>
            <li class="nav-item dropdown">
              <a
                class="nav-link dropdown-toggle"
                href="#"
                id="dropdown01"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
                >About</a
              >
              <div class="dropdown-menu" aria-labelledby="dropdown01">
                <a class="dropdown-item" href="our-company.html">Our Company</a>
                <a class="dropdown-item" href="our-team.html">Our Team</a>
                <a class="dropdown-item" href="biometrics.html"
                  >What are Biometrics?</a
                >
                <a class="dropdown-item" href="intellectual-property.html"
                  >Our Intellectual Property</a
                >
                <!-- <a class="dropdown-item " href="portfolio.html">Portfolio</a> -->
              </div>
            </li>
            <li class="nav-item dropdown">
              <a
                class="nav-link dropdown-toggle"
                href="#"
                id="dropdown02"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
                >Services</a
              >
              <div class="dropdown-menu" aria-labelledby="dropdown02">
                <a class="dropdown-item" href="technology.html">Technology</a>
                <a class="dropdown-item" href="industries.html">Industries</a>
              </div>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="licensing.html">Licensing</a>
            </li>
            <li class="nav-item active bold">
              <a class="nav-link" href="contact.php">Contact</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <main role="main">
      <div class="jumbotron sub-banner">
        <div class="container">
          <div class="row">
            <div class="col-md-12 text-center">
              <h1
                style="color: #fff"
                class="animate__animated animate__fadeIn animate__delay-1s"
              >
                Contact Us
              </h1>
            </div>
          </div>
        </div>
      </div>

      <div class="container">
        <div class="row justify-content-md-center align-items-center mt-5 py-5">
          <div class="col-md-5" data-aos="fade-up">
            <a href="tel: (561) 768-4925" class="text-link-purple d-flex">
              <i class="fas fa-phone-alt"></i>
              <p class="ml-3">(561) 768-4925</p>
            </a>

            <a
              href="https://www.google.com/maps/place/1112+NE+Industrial+Blvd,+Jensen+Beach,+FL+34957,+USA/@27.2256794,-80.2404628,17z/data=!3m1!4b1!4m5!3m4!1s0x88dee79bfd0ffd5f:0xd4d9769a3154965b!8m2!3d27.2256794!4d-80.2382741"
              class="text-link-purple d-flex"
            >
              <i class="fas fa-map-marker-alt"></i>
              <p class="ml-3">
                1112 NE Industrial Blvd, Jensen Beach, FL 34957
              </p>
            </a>
          </div>
        </div>

        <div class="row justify-content-md-center align-items-center pb-4">
          <div class="col-md-6" data-aos="fade-up">
            <p data-aos="fade-up">
              APS provides an innovative and game-changing software platform for
              merchants, financial institutions and health care companies that
              reduces operating cost, increases the efficiency of their service
              and expands their addressable markets.<br /><br />

              For more information or to learn about how APS can drive greater
              security in preventing consumer fraud, please fill in the form
              below.
            </p>
          </div>
        </div>

        <div class="row justify-content-md-center align-items-center mb-5 pb-5">
          <div class="col-md-6 contact-form" data-aos="fade-up">
            <h3 class="pb-3">Send Us A Message</h3>

            <form id = "idFrmInquiry">
              <div class="form-row">
                <div class="form-group col-md-6">
                  <label class="label-custom" for="fname">First Name</label>
                  <input
                    type="text"
                    class="form-control"
                    id="fname"
                    placeholder=""
                    name = "fname"
                  />
                </div>
                <div class="form-group col-md-6">
                  <label class="label-custom" for="lname">Last Name</label>
                  <input
                    type="text"
                    class="form-control"
                    id="lname"
                    placeholder=""
                    name = "lname"
                  />
                </div>
              </div>
              <div class="form-group">
                <label class="label-custom" for="email">Email Address</label>
                <input
                  type="email"
                  class="form-control"
                  id="email"
                  placeholder=""
                  name = "email"
                />
              </div>

              <div class="form-group">
                <label class="label-custom" for="phone">Phone Number</label>
                <input
                  type="tel"
                  class="form-control"
                  id="phone"
                  placeholder=""
                  name = "phone"
                />
              </div>
              <div class="form-group pb-2">
                <label class="label-custom" for="message">Message</label>
                <textarea class="form-control" id="message" rows="5" name = "message"></textarea>
              </div>

              <!-- recaptcha -->
              <div class="mb-3 wrapper-recaptcha">
                  <div class="g-recaptcha" data-sitekey="6LeevtoZAAAAAHANV1lqhrrhBbbQg81S1RRJXON6"
                    ></div>
                  <input type="hidden" class="hiddenRecaptcha" required name="hiddenRecaptcha" id="hiddenRecaptcha">
              </div>

              <div class="container-error"
                  style = "
                      background-color: #EEEEEE;
                      border: 1px dotted red;
                      padding: 15px;
                      margin-top:15px;
                      margin-bottom:15px;
                      
                      display: none;
                  "
                  >
                  <h5>Form Submission failed.</h5>
                  <ul>
                      <li style = "list-style-type: disc;">
                          <label for="fname" class="error" style = "display: inline;font-style:italic;color:red;">
                              Please enter First name
                          </label>
                          </li>
                      <li style = "list-style-type: disc;">
                          <label for="lname" class="error" style = "display: inline;font-style:italic;color:red;">
                              Please enter Lastname
                          </label>
                      </li>
                      <li style = "list-style-type: disc;">
                          <label for="email" class="error" style = "display: inline;font-style:italic;color:red;">
                              Please enter Email Address
                          </label>
                      </li>
                      <li style = "list-style-type: disc;">
                          <label for="phone" class="error" style = "display: inline;font-style:italic;color:red;">
                              Please enter Phone Number
                          </label>
                      </li>
                      <li style = "list-style-type: disc;">
                          <label for="message" class="error" style = "display: inline;font-style:italic;color:red;">
                              Please enter Message
                          </label>
                      </li>
                      <li style = "list-style-type: disc;">
                          <label for="hiddenRecaptcha" class="error" style = "display: inline;font-style:italic;color:red;">
                              Please check Recaptcha
                          </label>
                      </li>
                  </ul>
              </div>
              
              <button
                style="width: 100%; padding: 16px"
                class="btn gradient-button gradient-button-2 btn-md bold pl-4"
                type = "submit"
                >Submit</button>
            </form>
          </div>
        </div>
      </div>

      <hr />

      <div class="section cta-section">
        <div class="container pt-5 pb-5">
          <div
            class="row justify-content-md-center align-items-center pt-5 pb-5"
          >
            <div
              class="col-md-7 col-md-offset-1 order-md-12"
              data-aos="fade-up"
              data-aos-duration="2500"
            >
              <img src="images/cta-img.png" class="img-fluid img-center" />
            </div>
            <div class="col-md-5 py-4 order-md-1">
              <h2 class="light-purple pb-4" data-aos="fade-up">
                Stop Fraud Now.
              </h2>

              <p class="pb-4" data-aos="fade-up">
                For more information on our licensing programs. Contact us
                today!
              </p>

              <p data-aos="fade-up" data-aos-duration="2000">
                <a
                  class="btn gradient-button gradient-button-2 btn-md bold pl-4"
                  href="contact.php"
                  role="button"
                  >Contact Us<img src="images/btn-right-arrow.svg" class="ml-5"
                /></a>
              </p>
            </div>
          </div>
        </div>
      </div>

      <div class="section footer-section">
        <div class="container pt-5 pb-5">
          <div class="row justify-content-md-center pt-5">
            <div class="col-md-3 pb-5">
              <img src="images/logo-white-aps.svg" class="img-center" />
              <p class="text-white mt-4 text-center">
                Advanced Platform Solutions
              </p>
            </div>

            <div class="col-md-1"></div>

            <div class="col-md-3 mb-5">
              <a href="biometrics.html" class="text-link-white">
                <p>What are Biometrics?</p>
              </a>

              <a href="our-company.html" class="text-link-white">
                <p>Our Company</p>
              </a>

              <a href="intellectual-property.html" class="text-link-white">
                <p>Our Intellectual Property</p>
              </a>

              <a href="our-team.html" class="text-link-white">
                <p>Our Team</p>
              </a>

              <!-- <a href="portfolio.html" class="text-link-white">
							<p>Our Portfolio</p>
						</a> -->
            </div>

            <div class="col-md-2 mb-5">
              <a href="industries.html" class="text-link-white">
                <p>Industries</p>
              </a>

              <a href="licensing.html" class="text-link-white">
                <p>Licensing</p>
              </a>

              <a href="contact.php" class="text-link-white">
                <p>Contact Us</p>
              </a>
            </div>

            <div class="col-md-3 mb-5">
              <a href="tel: (561) 768-4925" class="text-link-white d-flex">
                <i class="fas fa-phone-alt"></i>
                <p class="ml-3">(561) 768-4925</p>
              </a>

              <a
                href="mailto: info@apssolution.com"
                class="text-link-white d-flex"
              >
                <i class="fas fa-envelope" style="margin-top: 3px"></i>
                <p class="ml-3">info@apssolution.com</p>
              </a>

              <div class="mt-3 hidden">
                <a href="#" class="text-link-white-alt pt-5">
                  <i class="fab fa-facebook-f mr-3"></i
                ></a>

                <a href="#" class="text-link-white-alt mt-3">
                  <i class="fab fa-instagram mr-3"></i
                ></a>

                <a href="#" class="text-link-white-alt mt-3">
                  <i class="fab fa-twitter mr-3"></i
                ></a>

                <a href="#" class="text-link-white-alt mt-3">
                  <i class="fab fa-linkedin"></i>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="section">
        <div class="container py-2">
          <div class="row justify-content-md-center">
            <div class="col-md-6">
              <p class="my-2">Advanced Platform Solutions</p>
            </div>

            <div class="col-md-3"></div>

            <div class="col-md-3">
              <a href="privacy.html">
                <p class="my-2 text-right text-right-alt terms-link">
                  Privacy Policy
                </p>
              </a>
            </div>
          </div>
        </div>
      </div>
    </main>

    <!-- integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" -->
    <!-- crossorigin="anonymous" -->
    <script
      src="https://code.jquery.com/jquery-3.5.1.min.js"
      
    ></script>
    <script>
      window.jQuery ||
        document.write(
          '<script src="../assets/js/vendor/jquery.slim.min.js"><\/script>'
        );
    </script>
    <script src="js/bootstrap.bundle.min.js"></script>

    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script>
      AOS.init();
    </script>

    <!-- added -->
    <script nonce src="https://www.google.com/recaptcha/api.js"></script>
    <script src="js/alphanum.js"></script>
    <script src="js/jquery.validate.js"></script>
    <script src="js/intlTelInput.js"></script>
    
    <!-- myscript -->
    <script>
      $(document).ready(function() {

        const validateAll = () => {
            if($("#idFrmInquiry").length){
                // custom validation
                $.validator.addMethod("noSpace",(value,element)=>{
                return value === '' || value.trim().length != 0
                },"Spaces are not allowed");

                $.validator.addMethod("phoneValidation",(value,element)=>{
                var isValid = true;
                if($(element).hasClass('has-error')){
                    isValid = false;
                }
                return isValid;
                },"Invalid Phone Number");

                $('#idFrmInquiry').validate({
                // render error message corresponds on each fields
                // errorPlacement: function(error, element) {
                //   error.appendTo(element.closest("div"));
                // },

                // one div for all message
                errorContainer: $('div.container-error'),
                errorLabelContainer: $("ul", $('div.container-error')),
                wrapper: 'li',
                
                // to include the hidden field recaptcha for validation
                ignore: ".ignore",

                rules:{
                    fname:{
                    required:true,
                    noSpace:true
                    },
                    lname:{
                    required:true,
                    noSpace:true,
                    },
                    email:{
                    required:true,
                    email:true,
                    noSpace:true,
                    },
                    phone:{
                    required:true,
                    noSpace:true,
                    phoneValidation:true
                    },
                    message:{
                    required:true,
                    noSpace:true
                    },
                    hiddenRecaptcha: {
                      required: function () {
                          if (grecaptcha.getResponse() == '') {
                              return true;
                          } else {
                              return false;
                          }
                      }
                    }
                },
                // for customize the error message
                messages:{
                    fname:{
                    required:"Please enter First name"
                    },
                    lname:{
                    required:"Please enter Lastname"
                    },
                    email:{
                    required:"Please enter Email Address"
                    },
                    phone:{
                    required:"Please enter Phone Number"
                    },
                    message:{
                    required:"Please enter Message"
                    },
                    hiddenRecaptcha:{
                      required:"Please check Recaptcha"
                    }
                }
                });
            }
        }

        validateAll();

        // for phone
        $('#phone').alphanum({
            allowLatin : false,
            allowOtherCharSets : false,
            allow : '+,()- ',    // Allow extra characters
        });

        var input = document.querySelector("#phone");

        // initialise plugin
        var iti = window.intlTelInput(input, {
            // get current location of the user
            // initialCountry: "auto",
            // geoIpLookup: function(callback) {
            //     $.get('https://ipinfo.io', function() {}, "jsonp").always(function(resp) {
            //     var countryCode = (resp && resp.country) ? resp.country : "";
            //     callback(countryCode);
            //     });
            // },
            utilsScript: "./js/utils.js"
        });

        var reset = function() {
            input.classList.remove("has-error");
        };

        // on blur: validate
        input.addEventListener('blur', function() {
            reset();
            if (input.value.trim()) {
                if (!iti.isValidNumber()) {
                input.classList.add("has-error");
                }
            }
        });

        // on keyup / change flag: reset
        input.addEventListener('change', reset);
        input.addEventListener('keyup', reset);
        // end of for phone

        $("#idFrmInquiry").on('submit', function(event) {

            var thisForm = $("#idFrmInquiry");

            if(!$(thisForm).valid()){
                return false;
            }
            event.preventDefault();

            var has_phone_error = 0;

            if($("#phone").hasClass("has-error")){
                has_phone_error = 1;
            }

            $.ajax({
                type: "POST",
                url: "./process.php",
                beforeSend:function(){
                    $("#idFrmInquiry button").html("Please Wait...").attr("disabled",true);
                },
                data: thisForm.serialize()+"&has_phone_error="+has_phone_error,
                success: function (response) {
                    grecaptcha.reset();
                    // console.log(response);
                    
                    var decodeResponse = JSON.parse(response);

                    if (!decodeResponse.is_success){
                        alert(decodeResponse.message);
                    }else if(decodeResponse.is_success){
                        window.location = "./thank-you.html";
                    }else{
                        alert(response);
                    }

                    $("#idFrmInquiry button").html('Submit').attr("disabled",false);

                }
            });

        });
        
      });
    </script>

  </body>
</html>
