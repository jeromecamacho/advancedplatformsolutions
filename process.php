<?php

  include_once 'PHPMailer/src/Exception.php';
  include_once 'PHPMailer/src/PHPMailer.php';
  include_once 'PHPMailer/src/SMTP.php';

  use PHPMailer\PHPMailer\PHPMailer;
  use PHPMailer\PHPMailer\Exception;

  $arrError = [];
  $postData = [];
  $postDataValidFields = [];

  $toExclude = ['hiddenRecaptcha','has_phone_error','g-recaptcha-response'];
  
  foreach($_POST as $keyPost => $valuePost){
    $postData[$keyPost] = $valuePost;

    if(in_array($keyPost,$toExclude)){
      continue;
    }

    if(trim($valuePost) == ""){
      $arrError[] = ucfirst($keyPost) . " is required.";
    }
    $postDataValidFields[$keyPost] = $valuePost;
  }

  // echo "<pre>";
  // print_r($postDataValidFields);
  // echo "</pre>";

  function _goEmail($postProperty){
    $smtpEmail = "apssolution2020@gmail.com";
    $smtpPassword = "Advanc3pl@tf0rmS0lution";
    $subject = "Advanced PlatForm Solution (Inquiry)";

    $body =
      "FirstName : " . $postProperty['fname'] . "<br>" .
      "LastName : " . $postProperty['lname'] . "<br>" .
      "Email : " . $postProperty['email'] . "<br>" .
      "Phone : " . $postProperty['phone'] . "<br>" .
      "Message : " . $postProperty['message'];

    $arrReceiver = [
      "christopher@apssolution.com",

      "jerome@directworksmedia.com"
    ];
    $withAttachment = false;

    if(empty(trim($postProperty['email']))){
      return json_encode(array("is_success" => false, "message" => "Email is required."));
    }

    $mail = new PHPMailer(true);
    $mail->isSMTP();
    $mail->Mailer = "smtp";
    // $mail->SMTPDebug  = 2;
    $mail->Host = "smtp.gmail.com";
    $mail->Port = 587;
    $mail->SMTPSecure = 'tls';
    $mail->SMTPAuth = true;

    $mail->Username =  $smtpEmail;
    $mail->Password = $smtpPassword;

    if(count($arrReceiver) > 0){
      foreach($arrReceiver as $value) {
        $mail->addAddress($value);
      }
    }

    $mail->addReplyTo($postProperty['email']);
    $mail->setFrom($smtpEmail, $subject);

    $mail->Subject = $subject;
    $mail->Body = $body;

    if($withAttachment){
      // $mail->addAttachment(BASE_PATH.'/pdf/Exclusive-Lifestyle-Program.pdf');
    }

    $mail->IsHTML(true);

    if (!$mail->Send()) {
      echo json_encode(array("is_success" => false, "message" => $mail->ErrorInfo));
    } else {
      echo json_encode(['is_success'=> true]);
    }
  }


  function recaptchaResponse($urlGoogle) {
    $ch = @curl_init();
    @curl_setopt($ch, CURLOPT_POST, true);
    @curl_setopt($ch, CURLOPT_URL, $urlGoogle);
    @curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    @curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response = @curl_exec($ch);
    $status_code = @curl_getinfo($ch, CURLINFO_HTTP_CODE);
    $curl_errors = curl_error($ch);
    @curl_close($ch);
    $decodedResponse = json_decode($response);

    echo json_encode(['is_success'=> false, "message" => $response]);

    return $decodedResponse;
  }
  
  
  if(!filter_var(trim($postDataValidFields['email']), FILTER_VALIDATE_EMAIL) && !empty(trim($postDataValidFields['email']))) {
    $arrError[] = "Email is Invalid";
  }
  
  if($postData['has_phone_error'] == 1 && !empty(trim($postDataValidFields['phone']))){
    $arrError[] = "Invalid Phone Number";
  }
  
  if (trim($postData['g-recaptcha-response']) === "") {
    $arrError[] = "Please check the captcha form";
  } else {
    $secretRecaptcha = "6LeevtoZAAAAAPPTMLukVumd4QvDpJ6z5ve3n3L4";
    
    // $urlGoogle = "https://www.google.com/recaptcha/api/siteverify?secret=".$secretRecaptcha."&response=".$postData['g-recaptcha-response'];
    // $getGoogleResponse = recaptchaResponse($urlGoogle);

    // if ($getGoogleResponse->success != 1) {
    //   $arrError[] = "Recaptcha Invalid, Please Try Again";
    // }

    // new
    $captcha_data = array(
      'secret' => $secretRecaptcha,
      'response' => $postData['g-recaptcha-response'],
      'remoteip' => $_SERVER['REMOTE_ADDR']
    );
    $url = 'https://www.google.com/recaptcha/api/siteverify';
    $ch = curl_init($url);

    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $captcha_data);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $response = curl_exec($ch);
    $result = json_decode($response,true);
    curl_close($ch);
    $json_response = json_decode($response);

    if($result['success'] != true) {
      $arrError[] = "Recaptcha Invalid, Please Try Again";
    }
    // end of new

    // $response = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secretRecaptcha."&response=" . $postData['g-recaptcha-response']), true);
    // if ($response['success'] != 1) {
    //   $this->arrError[] = "Recaptcha Invalid, Please Try Again";
    // }

  }

  if(count($arrError) > 0){
    echo json_encode(['is_success'=> false, "message" => implode("\n",$arrError)]);
  }else{
    // return _goHubspot("c930d763-4c55-496c-b222-9cd4bfeeae92",$postDataValidFields);
    return _goEmail($postDataValidFields);
    // echo json_encode(['is_success'=> true]);

  }
?>